User Interface
==============

When VECTO starts the [Main Form](#main-form) is loaded. Closing this form will close VECTO even if other dialogs are still open.

-	[Main Form](#main-form)
-   [Settings](#settings)
-	[Job Editor](#job-editor)
-	[Vehicle Editor - General](#vehicle-editor-general-tab)
-	[Vehicle Editor - Powertrain](#vehicle-editor-powertrain-tab)
-	[Vehicle Editor - Electric Machine](#vehicle-editor-electric-machine-tab)
-	[Vehicle Editor - REESS](#vehicle-editor-reess-tab)
-	[Vehicle Editor - IEPC](#vehicle-editor-iepc-tab)
-	[Vehicle Editor - IHPC](#vehicle-editor-ihpc-tab)
-	[Vehicle Editor - GenSet](#vehicle-editor-genset-tab)
-	[Vehicle Editor - Torque Limits](#vehicle-editor-torque-limits-tab)
-	[Vehicle Editor - ADAS](#vehicle-editor-adas-tab)
-	[Vehicle Editor - PTO](#vehicle-editor-pto-tab)
-   [Aux Dialog](#auxiliary-dialog)
-   [BusAux Dialog](#busauxiliary-dialog)
-	[Engine Editor](#engine-editor)
-	[Gearbox Editor](#gearbox-editor)
-	[Graph Window](#graph-window)

