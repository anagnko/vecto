﻿
Namespace Hvac


Public Interface IM1_AverageHVACLoadDemand

Function AveragePowerDemandAtCrankFromHVACMechanicalsWatts() As Single

Function AveragePowerDemandAtAlternatorFromHVACElectricsWatts() As Single

Function AveragePowerDemandAtCrankFromHVACElectricsWatts() As Single

Function HVACFuelingLitresPerHour() As Single

End Interface

End Namespace


