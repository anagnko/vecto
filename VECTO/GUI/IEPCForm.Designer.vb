﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class IEPCForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IEPCForm))
        Me.btFLCurveFile1 = New System.Windows.Forms.Button()
        Me.tbThermalOverload = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.tbInertia = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.tbModel = New System.Windows.Forms.TextBox()
        Me.tcVoltageLevels = New System.Windows.Forms.TabControl()
        Me.tpFirstVoltageLevel = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.tbFLCurve1 = New System.Windows.Forms.TextBox()
        Me.btShowFLCurve1 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.tbOverloadTime1 = New System.Windows.Forms.TextBox()
        Me.tbVoltage1 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbContinousTorque1 = New System.Windows.Forms.TextBox()
        Me.tbContinousTorqueSpeed1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.tboverloadTorqueSpeed1 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.tbOverloadTorque1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.lvPowerMap1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.tpVoltageLevel = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lvPowerMap2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.tbFLCurve2 = New System.Windows.Forms.TextBox()
        Me.btFLCurveFile2 = New System.Windows.Forms.Button()
        Me.btShowFLCurve2 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tbOverloadTime2 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.tbVoltage2 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.tbContinousTorque2 = New System.Windows.Forms.TextBox()
        Me.tbContinousTorqueSpeed2 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.tbOverloadTorqueSpeed2 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.tbOverloadTorque2 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cbDesignTypeWheelMotor = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbDifferentialIncluded = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btAddGear = New System.Windows.Forms.Button()
        Me.btRemoveGear = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lvGear = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lvDragCurve = New System.Windows.Forms.ListView()
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btAddDragCurve = New System.Windows.Forms.Button()
        Me.btRemoveDragCurve = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripBtNew = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripBtOpen = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripBtSave = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripBtSaveAs = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripBtSendTo = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.ButCancel = New System.Windows.Forms.Button()
        Me.ButOK = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pnInertia = New System.Windows.Forms.Panel()
        Me.lblinertiaUnit = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.Panel()
        Me.pnThermalOverloadRecovery = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LbStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.CmOpenFile = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenWithToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowInFolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.pnRatedPower = New System.Windows.Forms.Panel()
        Me.lblRatedPower = New System.Windows.Forms.Label()
        Me.lblRatedPowerUnit = New System.Windows.Forms.Label()
        Me.tbRatedPower = New System.Windows.Forms.TextBox()
        Me.pnElectricMachineType = New System.Windows.Forms.Panel()
        Me.cbEmType = New System.Windows.Forms.ComboBox()
        Me.lblEmType = New System.Windows.Forms.Label()
        Me.cbNumWheelMotors = New System.Windows.Forms.ComboBox()
        Me.tcVoltageLevels.SuspendLayout
        Me.tpFirstVoltageLevel.SuspendLayout
        Me.FlowLayoutPanel9.SuspendLayout
        Me.TableLayoutPanel1.SuspendLayout
        Me.GroupBox2.SuspendLayout
        Me.TableLayoutPanel6.SuspendLayout
        Me.tpVoltageLevel.SuspendLayout
        Me.GroupBox4.SuspendLayout
        Me.TableLayoutPanel2.SuspendLayout
        Me.FlowLayoutPanel10.SuspendLayout
        Me.TableLayoutPanel4.SuspendLayout
        Me.GroupBox1.SuspendLayout
        Me.TableLayoutPanel5.SuspendLayout
        Me.FlowLayoutPanel2.SuspendLayout
        Me.GroupBox3.SuspendLayout
        Me.TableLayoutPanel3.SuspendLayout
        Me.FlowLayoutPanel11.SuspendLayout
        Me.ToolStrip1.SuspendLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.FlowLayoutPanel1.SuspendLayout
        Me.FlowLayoutPanel4.SuspendLayout
        Me.FlowLayoutPanel5.SuspendLayout
        Me.pnInertia.SuspendLayout
        Me.FlowLayoutPanel7.SuspendLayout
        Me.pnThermalOverloadRecovery.SuspendLayout
        Me.StatusStrip1.SuspendLayout
        Me.CmOpenFile.SuspendLayout
        Me.pnRatedPower.SuspendLayout
        Me.pnElectricMachineType.SuspendLayout
        Me.SuspendLayout
        '
        'btFLCurveFile1
        '
        Me.btFLCurveFile1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btFLCurveFile1.Image = CType(resources.GetObject("btFLCurveFile1.Image"),System.Drawing.Image)
        Me.btFLCurveFile1.Location = New System.Drawing.Point(425, 1)
        Me.btFLCurveFile1.Margin = New System.Windows.Forms.Padding(0)
        Me.btFLCurveFile1.Name = "btFLCurveFile1"
        Me.btFLCurveFile1.Size = New System.Drawing.Size(24, 24)
        Me.btFLCurveFile1.TabIndex = 15
        Me.btFLCurveFile1.UseVisualStyleBackColor = true
        '
        'tbThermalOverload
        '
        Me.tbThermalOverload.Location = New System.Drawing.Point(190, 1)
        Me.tbThermalOverload.Margin = New System.Windows.Forms.Padding(30, 3, 3, 3)
        Me.tbThermalOverload.Name = "tbThermalOverload"
        Me.tbThermalOverload.Size = New System.Drawing.Size(56, 20)
        Me.tbThermalOverload.TabIndex = 3
        '
        'Label52
        '
        Me.Label52.AutoSize = true
        Me.Label52.Location = New System.Drawing.Point(5, 3)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(173, 13)
        Me.Label52.TabIndex = 50
        Me.Label52.Text = "Thermal Overload Recovery Factor"
        '
        'Label48
        '
        Me.Label48.AutoSize = true
        Me.Label48.Location = New System.Drawing.Point(3, 4)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(207, 13)
        Me.Label48.TabIndex = 48
        Me.Label48.Text = "Nr of Design Type Wheel Motor Measured"
        '
        'Label51
        '
        Me.Label51.AutoSize = true
        Me.Label51.Location = New System.Drawing.Point(4, 6)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(36, 13)
        Me.Label51.TabIndex = 45
        Me.Label51.Text = "Inertia"
        '
        'tbInertia
        '
        Me.tbInertia.Location = New System.Drawing.Point(190, 3)
        Me.tbInertia.Name = "tbInertia"
        Me.tbInertia.Size = New System.Drawing.Size(57, 20)
        Me.tbInertia.TabIndex = 2
        '
        'Label49
        '
        Me.Label49.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label49.AutoSize = true
        Me.Label49.Location = New System.Drawing.Point(3, 6)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(87, 13)
        Me.Label49.TabIndex = 43
        Me.Label49.Text = "Make and Model"
        '
        'tbModel
        '
        Me.tbModel.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.tbModel.Location = New System.Drawing.Point(96, 3)
        Me.tbModel.Margin = New System.Windows.Forms.Padding(3, 3, 0, 3)
        Me.tbModel.Name = "tbModel"
        Me.tbModel.Size = New System.Drawing.Size(414, 20)
        Me.tbModel.TabIndex = 1
        '
        'tcVoltageLevels
        '
        Me.tcVoltageLevels.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.tcVoltageLevels.Controls.Add(Me.tpFirstVoltageLevel)
        Me.tcVoltageLevels.Controls.Add(Me.tpVoltageLevel)
        Me.tcVoltageLevels.Location = New System.Drawing.Point(16, 213)
        Me.tcVoltageLevels.Name = "tcVoltageLevels"
        Me.tcVoltageLevels.SelectedIndex = 0
        Me.tcVoltageLevels.Size = New System.Drawing.Size(517, 322)
        Me.tcVoltageLevels.TabIndex = 107
        '
        'tpFirstVoltageLevel
        '
        Me.tpFirstVoltageLevel.Controls.Add(Me.FlowLayoutPanel9)
        Me.tpFirstVoltageLevel.Controls.Add(Me.TableLayoutPanel1)
        Me.tpFirstVoltageLevel.Controls.Add(Me.GroupBox2)
        Me.tpFirstVoltageLevel.Location = New System.Drawing.Point(4, 22)
        Me.tpFirstVoltageLevel.Name = "tpFirstVoltageLevel"
        Me.tpFirstVoltageLevel.Padding = New System.Windows.Forms.Padding(3, 3, 3, 3)
        Me.tpFirstVoltageLevel.Size = New System.Drawing.Size(509, 296)
        Me.tpFirstVoltageLevel.TabIndex = 0
        Me.tpFirstVoltageLevel.Text = "Voltage Level Low"
        Me.tpFirstVoltageLevel.UseVisualStyleBackColor = true
        '
        'FlowLayoutPanel9
        '
        Me.FlowLayoutPanel9.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.FlowLayoutPanel9.Controls.Add(Me.Label44)
        Me.FlowLayoutPanel9.Controls.Add(Me.tbFLCurve1)
        Me.FlowLayoutPanel9.Controls.Add(Me.btFLCurveFile1)
        Me.FlowLayoutPanel9.Controls.Add(Me.btShowFLCurve1)
        Me.FlowLayoutPanel9.Location = New System.Drawing.Point(6, 94)
        Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
        Me.FlowLayoutPanel9.Size = New System.Drawing.Size(496, 26)
        Me.FlowLayoutPanel9.TabIndex = 108
        '
        'Label44
        '
        Me.Label44.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label44.AutoSize = true
        Me.Label44.Location = New System.Drawing.Point(14, 6)
        Me.Label44.Margin = New System.Windows.Forms.Padding(14, 0, 0, 0)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(125, 13)
        Me.Label44.TabIndex = 59
        Me.Label44.Text = "Full Load Curve (.viepcp)"
        '
        'tbFLCurve1
        '
        Me.tbFLCurve1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbFLCurve1.Location = New System.Drawing.Point(142, 3)
        Me.tbFLCurve1.Margin = New System.Windows.Forms.Padding(3, 3, 6, 3)
        Me.tbFLCurve1.Name = "tbFLCurve1"
        Me.tbFLCurve1.Size = New System.Drawing.Size(277, 20)
        Me.tbFLCurve1.TabIndex = 14
        '
        'btShowFLCurve1
        '
        Me.btShowFLCurve1.Image = Global.TUGraz.VECTO.My.Resources.Resources.application_export_icon_small
        Me.btShowFLCurve1.Location = New System.Drawing.Point(449, 1)
        Me.btShowFLCurve1.Margin = New System.Windows.Forms.Padding(0, 1, 0, 0)
        Me.btShowFLCurve1.Name = "btShowFLCurve1"
        Me.btShowFLCurve1.Size = New System.Drawing.Size(24, 24)
        Me.btShowFLCurve1.TabIndex = 85
        Me.btShowFLCurve1.TabStop = false
        Me.btShowFLCurve1.UseVisualStyleBackColor = true
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 7
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label13, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label43, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label37, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tbOverloadTime1, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tbVoltage1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label39, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbContinousTorque1, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tbContinousTorqueSpeed1, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label15, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label42, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label14, 5, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tboverloadTorqueSpeed1, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label41, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tbOverloadTorque1, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 5, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 7)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(496, 82)
        Me.TableLayoutPanel1.TabIndex = 107
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label11.AutoSize = true
        Me.Label11.Location = New System.Drawing.Point(204, 34)
        Me.Label11.Margin = New System.Windows.Forms.Padding(0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(29, 13)
        Me.Label11.TabIndex = 63
        Me.Label11.Text = "[Nm]"
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(208, 7)
        Me.Label4.Margin = New System.Windows.Forms.Padding(0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 13)
        Me.Label4.TabIndex = 62
        Me.Label4.Text = "[V]"
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label13.AutoSize = true
        Me.Label13.Location = New System.Drawing.Point(428, 7)
        Me.Label13.Margin = New System.Windows.Forms.Padding(0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(18, 13)
        Me.Label13.TabIndex = 65
        Me.Label13.Text = "[s]"
        '
        'Label43
        '
        Me.Label43.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label43.AutoSize = true
        Me.Label43.Location = New System.Drawing.Point(280, 7)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(76, 13)
        Me.Label43.TabIndex = 11
        Me.Label43.Text = "Overload Time"
        '
        'Label37
        '
        Me.Label37.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label37.AutoSize = true
        Me.Label37.Location = New System.Drawing.Point(91, 7)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(43, 13)
        Me.Label37.TabIndex = 1
        Me.Label37.Text = "Voltage"
        '
        'tbOverloadTime1
        '
        Me.tbOverloadTime1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTime1.Location = New System.Drawing.Point(362, 3)
        Me.tbOverloadTime1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbOverloadTime1.Name = "tbOverloadTime1"
        Me.tbOverloadTime1.Size = New System.Drawing.Size(56, 20)
        Me.tbOverloadTime1.TabIndex = 11
        '
        'tbVoltage1
        '
        Me.tbVoltage1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbVoltage1.Location = New System.Drawing.Point(141, 3)
        Me.tbVoltage1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbVoltage1.Name = "tbVoltage1"
        Me.tbVoltage1.Size = New System.Drawing.Size(56, 20)
        Me.tbVoltage1.TabIndex = 8
        '
        'Label39
        '
        Me.Label39.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label39.AutoSize = true
        Me.Label39.Location = New System.Drawing.Point(37, 34)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(97, 13)
        Me.Label39.TabIndex = 3
        Me.Label39.Text = "Continuous Torque"
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(3, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Continuous Torque Speed"
        '
        'tbContinousTorque1
        '
        Me.tbContinousTorque1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinousTorque1.Location = New System.Drawing.Point(141, 30)
        Me.tbContinousTorque1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbContinousTorque1.Name = "tbContinousTorque1"
        Me.tbContinousTorque1.Size = New System.Drawing.Size(56, 20)
        Me.tbContinousTorque1.TabIndex = 9
        '
        'tbContinousTorqueSpeed1
        '
        Me.tbContinousTorqueSpeed1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinousTorqueSpeed1.Location = New System.Drawing.Point(141, 58)
        Me.tbContinousTorqueSpeed1.Margin = New System.Windows.Forms.Padding(0)
        Me.tbContinousTorqueSpeed1.Name = "tbContinousTorqueSpeed1"
        Me.tbContinousTorqueSpeed1.Size = New System.Drawing.Size(56, 20)
        Me.tbContinousTorqueSpeed1.TabIndex = 10
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label15.AutoSize = true
        Me.Label15.Location = New System.Drawing.Point(203, 61)
        Me.Label15.Margin = New System.Windows.Forms.Padding(0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(30, 13)
        Me.Label15.TabIndex = 67
        Me.Label15.Text = "[rpm]"
        '
        'Label42
        '
        Me.Label42.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label42.AutoSize = true
        Me.Label42.Location = New System.Drawing.Point(266, 55)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(90, 26)
        Me.Label42.TabIndex = 9
        Me.Label42.Text = "Overload Torque Speed"
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(422, 61)
        Me.Label14.Margin = New System.Windows.Forms.Padding(0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(30, 13)
        Me.Label14.TabIndex = 66
        Me.Label14.Text = "[rpm]"
        '
        'tboverloadTorqueSpeed1
        '
        Me.tboverloadTorqueSpeed1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tboverloadTorqueSpeed1.Location = New System.Drawing.Point(362, 58)
        Me.tboverloadTorqueSpeed1.Name = "tboverloadTorqueSpeed1"
        Me.tboverloadTorqueSpeed1.Size = New System.Drawing.Size(55, 20)
        Me.tboverloadTorqueSpeed1.TabIndex = 13
        '
        'Label41
        '
        Me.Label41.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label41.AutoSize = true
        Me.Label41.Location = New System.Drawing.Point(269, 34)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(87, 13)
        Me.Label41.TabIndex = 7
        Me.Label41.Text = "Overload Torque"
        '
        'tbOverloadTorque1
        '
        Me.tbOverloadTorque1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTorque1.Location = New System.Drawing.Point(362, 30)
        Me.tbOverloadTorque1.Name = "tbOverloadTorque1"
        Me.tbOverloadTorque1.Size = New System.Drawing.Size(55, 20)
        Me.tbOverloadTorque1.TabIndex = 12
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label12.AutoSize = true
        Me.Label12.Location = New System.Drawing.Point(422, 34)
        Me.Label12.Margin = New System.Windows.Forms.Padding(0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 13)
        Me.Label12.TabIndex = 64
        Me.Label12.Text = "[Nm]"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel6)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 126)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(315, 162)
        Me.GroupBox2.TabIndex = 60
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Power Map Per Gear"
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170!))
        Me.TableLayoutPanel6.Controls.Add(Me.Label47, 1, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.lvPowerMap1, 0, 0)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(6, 13)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 2
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.81119!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.18881!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(298, 143)
        Me.TableLayoutPanel6.TabIndex = 53
        '
        'Label47
        '
        Me.Label47.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label47.AutoSize = true
        Me.Label47.Location = New System.Drawing.Point(190, 127)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(106, 13)
        Me.Label47.TabIndex = 6
        Me.Label47.Text = "(Double-Click to Edit)"
        '
        'lvPowerMap1
        '
        Me.lvPowerMap1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lvPowerMap1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader13, Me.ColumnHeader14})
        Me.TableLayoutPanel6.SetColumnSpan(Me.lvPowerMap1, 2)
        Me.lvPowerMap1.FullRowSelect = true
        Me.lvPowerMap1.GridLines = true
        Me.lvPowerMap1.HideSelection = false
        Me.lvPowerMap1.Location = New System.Drawing.Point(3, 3)
        Me.lvPowerMap1.MultiSelect = false
        Me.lvPowerMap1.Name = "lvPowerMap1"
        Me.lvPowerMap1.Size = New System.Drawing.Size(293, 121)
        Me.lvPowerMap1.TabIndex = 108
        Me.lvPowerMap1.TabStop = false
        Me.lvPowerMap1.UseCompatibleStateImageBehavior = false
        Me.lvPowerMap1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Gear #"
        Me.ColumnHeader13.Width = 59
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Power Map Filename"
        Me.ColumnHeader14.Width = 223
        '
        'tpVoltageLevel
        '
        Me.tpVoltageLevel.Controls.Add(Me.GroupBox4)
        Me.tpVoltageLevel.Controls.Add(Me.FlowLayoutPanel10)
        Me.tpVoltageLevel.Controls.Add(Me.TableLayoutPanel4)
        Me.tpVoltageLevel.Location = New System.Drawing.Point(4, 22)
        Me.tpVoltageLevel.Name = "tpVoltageLevel"
        Me.tpVoltageLevel.Padding = New System.Windows.Forms.Padding(3, 3, 3, 3)
        Me.tpVoltageLevel.Size = New System.Drawing.Size(509, 296)
        Me.tpVoltageLevel.TabIndex = 1
        Me.tpVoltageLevel.Text = "Voltage Level High"
        Me.tpVoltageLevel.UseVisualStyleBackColor = true
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox4.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 126)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(315, 162)
        Me.GroupBox4.TabIndex = 75
        Me.GroupBox4.TabStop = false
        Me.GroupBox4.Text = "Power Map Per Gear"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.lvPowerMap2, 0, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(6, 13)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.81119!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.18881!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(298, 143)
        Me.TableLayoutPanel2.TabIndex = 53
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(190, 127)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(106, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "(Double-Click to Edit)"
        '
        'lvPowerMap2
        '
        Me.lvPowerMap2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lvPowerMap2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader7})
        Me.TableLayoutPanel2.SetColumnSpan(Me.lvPowerMap2, 2)
        Me.lvPowerMap2.FullRowSelect = true
        Me.lvPowerMap2.GridLines = true
        Me.lvPowerMap2.HideSelection = false
        Me.lvPowerMap2.Location = New System.Drawing.Point(3, 3)
        Me.lvPowerMap2.MultiSelect = false
        Me.lvPowerMap2.Name = "lvPowerMap2"
        Me.lvPowerMap2.Size = New System.Drawing.Size(293, 121)
        Me.lvPowerMap2.TabIndex = 109
        Me.lvPowerMap2.TabStop = false
        Me.lvPowerMap2.UseCompatibleStateImageBehavior = false
        Me.lvPowerMap2.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Gear #"
        Me.ColumnHeader6.Width = 59
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Power Map Filename"
        Me.ColumnHeader7.Width = 223
        '
        'FlowLayoutPanel10
        '
        Me.FlowLayoutPanel10.Controls.Add(Me.Label28)
        Me.FlowLayoutPanel10.Controls.Add(Me.tbFLCurve2)
        Me.FlowLayoutPanel10.Controls.Add(Me.btFLCurveFile2)
        Me.FlowLayoutPanel10.Controls.Add(Me.btShowFLCurve2)
        Me.FlowLayoutPanel10.Location = New System.Drawing.Point(6, 94)
        Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
        Me.FlowLayoutPanel10.Size = New System.Drawing.Size(496, 26)
        Me.FlowLayoutPanel10.TabIndex = 110
        '
        'Label28
        '
        Me.Label28.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label28.AutoSize = true
        Me.Label28.Location = New System.Drawing.Point(14, 6)
        Me.Label28.Margin = New System.Windows.Forms.Padding(14, 0, 0, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(125, 13)
        Me.Label28.TabIndex = 59
        Me.Label28.Text = "Full Load Curve (.viepcp)"
        '
        'tbFLCurve2
        '
        Me.tbFLCurve2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbFLCurve2.Location = New System.Drawing.Point(142, 3)
        Me.tbFLCurve2.Margin = New System.Windows.Forms.Padding(3, 3, 6, 3)
        Me.tbFLCurve2.Name = "tbFLCurve2"
        Me.tbFLCurve2.Size = New System.Drawing.Size(277, 20)
        Me.tbFLCurve2.TabIndex = 23
        '
        'btFLCurveFile2
        '
        Me.btFLCurveFile2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btFLCurveFile2.Image = CType(resources.GetObject("btFLCurveFile2.Image"),System.Drawing.Image)
        Me.btFLCurveFile2.Location = New System.Drawing.Point(425, 1)
        Me.btFLCurveFile2.Margin = New System.Windows.Forms.Padding(0)
        Me.btFLCurveFile2.Name = "btFLCurveFile2"
        Me.btFLCurveFile2.Size = New System.Drawing.Size(24, 24)
        Me.btFLCurveFile2.TabIndex = 24
        Me.btFLCurveFile2.UseVisualStyleBackColor = true
        '
        'btShowFLCurve2
        '
        Me.btShowFLCurve2.Image = Global.TUGraz.VECTO.My.Resources.Resources.application_export_icon_small
        Me.btShowFLCurve2.Location = New System.Drawing.Point(449, 1)
        Me.btShowFLCurve2.Margin = New System.Windows.Forms.Padding(0, 1, 0, 0)
        Me.btShowFLCurve2.Name = "btShowFLCurve2"
        Me.btShowFLCurve2.Size = New System.Drawing.Size(24, 24)
        Me.btShowFLCurve2.TabIndex = 85
        Me.btShowFLCurve2.TabStop = false
        Me.btShowFLCurve2.UseVisualStyleBackColor = true
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel4.ColumnCount = 7
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label17, 2, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label18, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label26, 5, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label19, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.tbOverloadTime2, 4, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label24, 3, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.tbVoltage2, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label20, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label21, 0, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.tbContinousTorque2, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.tbContinousTorqueSpeed2, 1, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label27, 2, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label23, 3, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.tbOverloadTorqueSpeed2, 4, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label25, 5, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.Label22, 3, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.tbOverloadTorque2, 4, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label16, 5, 1)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(6, 7)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 3
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(496, 82)
        Me.TableLayoutPanel4.TabIndex = 109
        '
        'Label17
        '
        Me.Label17.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label17.AutoSize = true
        Me.Label17.Location = New System.Drawing.Point(204, 34)
        Me.Label17.Margin = New System.Windows.Forms.Padding(0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(29, 13)
        Me.Label17.TabIndex = 63
        Me.Label17.Text = "[Nm]"
        '
        'Label18
        '
        Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label18.AutoSize = true
        Me.Label18.Location = New System.Drawing.Point(208, 7)
        Me.Label18.Margin = New System.Windows.Forms.Padding(0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(20, 13)
        Me.Label18.TabIndex = 62
        Me.Label18.Text = "[V]"
        '
        'Label26
        '
        Me.Label26.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label26.AutoSize = true
        Me.Label26.Location = New System.Drawing.Point(428, 7)
        Me.Label26.Margin = New System.Windows.Forms.Padding(0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(18, 13)
        Me.Label26.TabIndex = 65
        Me.Label26.Text = "[s]"
        '
        'Label19
        '
        Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label19.AutoSize = true
        Me.Label19.Location = New System.Drawing.Point(91, 7)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(43, 13)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Voltage"
        '
        'tbOverloadTime2
        '
        Me.tbOverloadTime2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTime2.Location = New System.Drawing.Point(362, 3)
        Me.tbOverloadTime2.Name = "tbOverloadTime2"
        Me.tbOverloadTime2.Size = New System.Drawing.Size(55, 20)
        Me.tbOverloadTime2.TabIndex = 20
        '
        'Label24
        '
        Me.Label24.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label24.AutoSize = true
        Me.Label24.Location = New System.Drawing.Point(280, 7)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(76, 13)
        Me.Label24.TabIndex = 11
        Me.Label24.Text = "Overload Time"
        '
        'tbVoltage2
        '
        Me.tbVoltage2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbVoltage2.Location = New System.Drawing.Point(141, 3)
        Me.tbVoltage2.Name = "tbVoltage2"
        Me.tbVoltage2.Size = New System.Drawing.Size(55, 20)
        Me.tbVoltage2.TabIndex = 17
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label20.AutoSize = true
        Me.Label20.Location = New System.Drawing.Point(37, 34)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(97, 13)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "Continuous Torque"
        '
        'Label21
        '
        Me.Label21.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label21.AutoSize = true
        Me.Label21.Location = New System.Drawing.Point(3, 61)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(131, 13)
        Me.Label21.TabIndex = 6
        Me.Label21.Text = "Continuous Torque Speed"
        '
        'tbContinousTorque2
        '
        Me.tbContinousTorque2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinousTorque2.Location = New System.Drawing.Point(141, 30)
        Me.tbContinousTorque2.Name = "tbContinousTorque2"
        Me.tbContinousTorque2.Size = New System.Drawing.Size(55, 20)
        Me.tbContinousTorque2.TabIndex = 18
        '
        'tbContinousTorqueSpeed2
        '
        Me.tbContinousTorqueSpeed2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbContinousTorqueSpeed2.Location = New System.Drawing.Point(141, 58)
        Me.tbContinousTorqueSpeed2.Name = "tbContinousTorqueSpeed2"
        Me.tbContinousTorqueSpeed2.Size = New System.Drawing.Size(55, 20)
        Me.tbContinousTorqueSpeed2.TabIndex = 19
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label27.AutoSize = true
        Me.Label27.Location = New System.Drawing.Point(203, 61)
        Me.Label27.Margin = New System.Windows.Forms.Padding(0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(30, 13)
        Me.Label27.TabIndex = 67
        Me.Label27.Text = "[rpm]"
        '
        'Label23
        '
        Me.Label23.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label23.AutoSize = true
        Me.Label23.Location = New System.Drawing.Point(266, 55)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(90, 26)
        Me.Label23.TabIndex = 9
        Me.Label23.Text = "Overload Torque Speed"
        '
        'tbOverloadTorqueSpeed2
        '
        Me.tbOverloadTorqueSpeed2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTorqueSpeed2.Location = New System.Drawing.Point(362, 58)
        Me.tbOverloadTorqueSpeed2.Name = "tbOverloadTorqueSpeed2"
        Me.tbOverloadTorqueSpeed2.Size = New System.Drawing.Size(55, 20)
        Me.tbOverloadTorqueSpeed2.TabIndex = 22
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label25.AutoSize = true
        Me.Label25.Location = New System.Drawing.Point(422, 61)
        Me.Label25.Margin = New System.Windows.Forms.Padding(0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(30, 13)
        Me.Label25.TabIndex = 66
        Me.Label25.Text = "[rpm]"
        '
        'Label22
        '
        Me.Label22.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label22.AutoSize = true
        Me.Label22.Location = New System.Drawing.Point(269, 34)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(87, 13)
        Me.Label22.TabIndex = 7
        Me.Label22.Text = "Overload Torque"
        '
        'tbOverloadTorque2
        '
        Me.tbOverloadTorque2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbOverloadTorque2.Location = New System.Drawing.Point(362, 30)
        Me.tbOverloadTorque2.Name = "tbOverloadTorque2"
        Me.tbOverloadTorque2.Size = New System.Drawing.Size(55, 20)
        Me.tbOverloadTorque2.TabIndex = 21
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label16.AutoSize = true
        Me.Label16.Location = New System.Drawing.Point(422, 34)
        Me.Label16.Margin = New System.Windows.Forms.Padding(0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 13)
        Me.Label16.TabIndex = 64
        Me.Label16.Text = "[Nm]"
        '
        'cbDesignTypeWheelMotor
        '
        Me.cbDesignTypeWheelMotor.AutoSize = true
        Me.cbDesignTypeWheelMotor.Location = New System.Drawing.Point(146, 5)
        Me.cbDesignTypeWheelMotor.Margin = New System.Windows.Forms.Padding(50, 6, 3, 6)
        Me.cbDesignTypeWheelMotor.Name = "cbDesignTypeWheelMotor"
        Me.cbDesignTypeWheelMotor.Size = New System.Drawing.Size(15, 14)
        Me.cbDesignTypeWheelMotor.TabIndex = 4
        Me.cbDesignTypeWheelMotor.UseVisualStyleBackColor = true
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(3, 4)
        Me.Label2.Margin = New System.Windows.Forms.Padding(3, 6, 6, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 13)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "Differential Included"
        '
        'cbDifferentialIncluded
        '
        Me.cbDifferentialIncluded.AutoSize = true
        Me.cbDifferentialIncluded.Location = New System.Drawing.Point(145, 5)
        Me.cbDifferentialIncluded.Margin = New System.Windows.Forms.Padding(80, 6, 3, 6)
        Me.cbDifferentialIncluded.Name = "cbDifferentialIncluded"
        Me.cbDifferentialIncluded.Size = New System.Drawing.Size(15, 14)
        Me.cbDifferentialIncluded.TabIndex = 6
        Me.cbDifferentialIncluded.UseVisualStyleBackColor = true
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel5)
        Me.GroupBox1.Location = New System.Drawing.Point(540, 78)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(314, 239)
        Me.GroupBox1.TabIndex = 61
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Gears"
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 171!))
        Me.TableLayoutPanel5.Controls.Add(Me.FlowLayoutPanel2, 0, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.Label6, 1, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.lvGear, 0, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(7, 19)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 2
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.61636!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.38365!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(300, 212)
        Me.TableLayoutPanel5.TabIndex = 111
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btAddGear)
        Me.FlowLayoutPanel2.Controls.Add(Me.btRemoveGear)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 169)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(61, 28)
        Me.FlowLayoutPanel2.TabIndex = 112
        '
        'btAddGear
        '
        Me.btAddGear.Image = Global.TUGraz.VECTO.My.Resources.Resources.plus_circle_icon
        Me.btAddGear.Location = New System.Drawing.Point(3, 3)
        Me.btAddGear.Name = "btAddGear"
        Me.btAddGear.Size = New System.Drawing.Size(24, 24)
        Me.btAddGear.TabIndex = 25
        Me.btAddGear.UseVisualStyleBackColor = true
        '
        'btRemoveGear
        '
        Me.btRemoveGear.Image = Global.TUGraz.VECTO.My.Resources.Resources.minus_circle_icon
        Me.btRemoveGear.Location = New System.Drawing.Point(33, 3)
        Me.btRemoveGear.Name = "btRemoveGear"
        Me.btRemoveGear.Size = New System.Drawing.Size(24, 24)
        Me.btRemoveGear.TabIndex = 26
        Me.btRemoveGear.UseVisualStyleBackColor = true
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = true
        Me.Label6.Location = New System.Drawing.Point(191, 166)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(106, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "(Double-Click to Edit)"
        '
        'lvGear
        '
        Me.lvGear.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lvGear.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.TableLayoutPanel5.SetColumnSpan(Me.lvGear, 2)
        Me.lvGear.FullRowSelect = true
        Me.lvGear.GridLines = true
        Me.lvGear.HideSelection = false
        Me.lvGear.Location = New System.Drawing.Point(3, 3)
        Me.lvGear.MultiSelect = false
        Me.lvGear.Name = "lvGear"
        Me.lvGear.Size = New System.Drawing.Size(294, 160)
        Me.lvGear.TabIndex = 78
        Me.lvGear.TabStop = false
        Me.lvGear.UseCompatibleStateImageBehavior = false
        Me.lvGear.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Ratio"
        Me.ColumnHeader1.Width = 45
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Max Out Shaft Torque"
        Me.ColumnHeader2.Width = 120
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Max Out Shaft Speed"
        Me.ColumnHeader3.Width = 120
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox3.Controls.Add(Me.TableLayoutPanel3)
        Me.GroupBox3.Location = New System.Drawing.Point(540, 325)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(314, 207)
        Me.GroupBox3.TabIndex = 62
        Me.GroupBox3.TabStop = false
        Me.GroupBox3.Text = "Drag Curves"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 171!))
        Me.TableLayoutPanel3.Controls.Add(Me.lvDragCurve, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.FlowLayoutPanel11, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Label7, 1, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(7, 19)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.61636!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.38365!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(300, 183)
        Me.TableLayoutPanel3.TabIndex = 113
        '
        'lvDragCurve
        '
        Me.lvDragCurve.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lvDragCurve.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4, Me.ColumnHeader5})
        Me.TableLayoutPanel3.SetColumnSpan(Me.lvDragCurve, 2)
        Me.lvDragCurve.FullRowSelect = true
        Me.lvDragCurve.GridLines = true
        Me.lvDragCurve.HideSelection = false
        Me.lvDragCurve.Location = New System.Drawing.Point(3, 3)
        Me.lvDragCurve.MultiSelect = false
        Me.lvDragCurve.Name = "lvDragCurve"
        Me.lvDragCurve.Size = New System.Drawing.Size(294, 137)
        Me.lvDragCurve.TabIndex = 78
        Me.lvDragCurve.TabStop = false
        Me.lvDragCurve.UseCompatibleStateImageBehavior = false
        Me.lvDragCurve.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Gear #"
        Me.ColumnHeader4.Width = 59
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Drag Curve Filename"
        Me.ColumnHeader5.Width = 225
        '
        'FlowLayoutPanel11
        '
        Me.FlowLayoutPanel11.Controls.Add(Me.btAddDragCurve)
        Me.FlowLayoutPanel11.Controls.Add(Me.btRemoveDragCurve)
        Me.FlowLayoutPanel11.Location = New System.Drawing.Point(3, 146)
        Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
        Me.FlowLayoutPanel11.Size = New System.Drawing.Size(61, 28)
        Me.FlowLayoutPanel11.TabIndex = 114
        '
        'btAddDragCurve
        '
        Me.btAddDragCurve.Image = Global.TUGraz.VECTO.My.Resources.Resources.plus_circle_icon
        Me.btAddDragCurve.Location = New System.Drawing.Point(3, 3)
        Me.btAddDragCurve.Name = "btAddDragCurve"
        Me.btAddDragCurve.Size = New System.Drawing.Size(24, 24)
        Me.btAddDragCurve.TabIndex = 27
        Me.btAddDragCurve.UseVisualStyleBackColor = true
        '
        'btRemoveDragCurve
        '
        Me.btRemoveDragCurve.Image = Global.TUGraz.VECTO.My.Resources.Resources.minus_circle_icon
        Me.btRemoveDragCurve.Location = New System.Drawing.Point(33, 3)
        Me.btRemoveDragCurve.Name = "btRemoveDragCurve"
        Me.btRemoveDragCurve.Size = New System.Drawing.Size(24, 24)
        Me.btRemoveDragCurve.TabIndex = 28
        Me.btRemoveDragCurve.UseVisualStyleBackColor = true
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = true
        Me.Label7.Location = New System.Drawing.Point(191, 143)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(106, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "(Double-Click to Edit)"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripBtNew, Me.ToolStripBtOpen, Me.ToolStripBtSave, Me.ToolStripBtSaveAs, Me.ToolStripSeparator3, Me.ToolStripBtSendTo, Me.ToolStripSeparator1, Me.ToolStripButton1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(864, 31)
        Me.ToolStrip1.TabIndex = 63
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripBtNew
        '
        Me.ToolStripBtNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtNew.Image = Global.TUGraz.VECTO.My.Resources.Resources.blue_document_icon
        Me.ToolStripBtNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtNew.Name = "ToolStripBtNew"
        Me.ToolStripBtNew.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtNew.Text = "ToolStripButton1"
        Me.ToolStripBtNew.ToolTipText = "New"
        '
        'ToolStripBtOpen
        '
        Me.ToolStripBtOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtOpen.Image = Global.TUGraz.VECTO.My.Resources.Resources.Open_icon
        Me.ToolStripBtOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtOpen.Name = "ToolStripBtOpen"
        Me.ToolStripBtOpen.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtOpen.Text = "ToolStripButton1"
        Me.ToolStripBtOpen.ToolTipText = "Open..."
        '
        'ToolStripBtSave
        '
        Me.ToolStripBtSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtSave.Image = Global.TUGraz.VECTO.My.Resources.Resources.Actions_document_save_icon
        Me.ToolStripBtSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtSave.Name = "ToolStripBtSave"
        Me.ToolStripBtSave.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtSave.Text = "ToolStripButton1"
        Me.ToolStripBtSave.ToolTipText = "Save"
        '
        'ToolStripBtSaveAs
        '
        Me.ToolStripBtSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtSaveAs.Image = Global.TUGraz.VECTO.My.Resources.Resources.Actions_document_save_as_icon
        Me.ToolStripBtSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtSaveAs.Name = "ToolStripBtSaveAs"
        Me.ToolStripBtSaveAs.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtSaveAs.Text = "ToolStripButton1"
        Me.ToolStripBtSaveAs.ToolTipText = "Save As..."
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripBtSendTo
        '
        Me.ToolStripBtSendTo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripBtSendTo.Image = Global.TUGraz.VECTO.My.Resources.Resources.export_icon
        Me.ToolStripBtSendTo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripBtSendTo.Name = "ToolStripBtSendTo"
        Me.ToolStripBtSendTo.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripBtSendTo.Text = "Send to Vehicle Editor"
        Me.ToolStripBtSendTo.ToolTipText = "Send to Vehicle Editor"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 31)
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.TUGraz.VECTO.My.Resources.Resources.Help_icon
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(28, 28)
        Me.ToolStripButton1.Text = "Help"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox1.Image = Global.TUGraz.VECTO.My.Resources.Resources.VECTO_Mainform
        Me.PictureBox1.Location = New System.Drawing.Point(0, 31)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(864, 40)
        Me.PictureBox1.TabIndex = 64
        Me.PictureBox1.TabStop = false
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitle.AutoSize = true
        Me.lblTitle.BackColor = System.Drawing.Color.White
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 18!)
        Me.lblTitle.Location = New System.Drawing.Point(114, 36)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(68, 29)
        Me.lblTitle.TabIndex = 65
        Me.lblTitle.Text = "IEPC"
        '
        'ButCancel
        '
        Me.ButCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ButCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButCancel.Location = New System.Drawing.Point(778, 538)
        Me.ButCancel.Name = "ButCancel"
        Me.ButCancel.Size = New System.Drawing.Size(75, 23)
        Me.ButCancel.TabIndex = 67
        Me.ButCancel.Text = "Cancel"
        Me.ButCancel.UseVisualStyleBackColor = true
        '
        'ButOK
        '
        Me.ButOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ButOK.Location = New System.Drawing.Point(697, 538)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(75, 23)
        Me.ButOK.TabIndex = 66
        Me.ButOK.Text = "Save"
        Me.ButOK.UseVisualStyleBackColor = true
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label49)
        Me.FlowLayoutPanel1.Controls.Add(Me.tbModel)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(12, 77)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(513, 26)
        Me.FlowLayoutPanel1.TabIndex = 101
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.FlowLayoutPanel4.Controls.Add(Me.Label3)
        Me.FlowLayoutPanel4.Controls.Add(Me.cbDesignTypeWheelMotor)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(12, 161)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(210, 24)
        Me.FlowLayoutPanel4.TabIndex = 104
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(4, 4)
        Me.Label3.Margin = New System.Windows.Forms.Padding(3, 6, 6, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(131, 13)
        Me.Label3.TabIndex = 55
        Me.Label3.Text = "Design Type Wheel Motor"
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.cbNumWheelMotors)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label48)
        Me.FlowLayoutPanel5.Controls.Add(Me.Label9)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(227, 161)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(298, 24)
        Me.FlowLayoutPanel5.TabIndex = 105
        '
        'Label9
        '
        Me.Label9.AutoSize = true
        Me.Label9.Location = New System.Drawing.Point(254, 4)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(16, 13)
        Me.Label9.TabIndex = 66
        Me.Label9.Text = "[-]"
        '
        'pnInertia
        '
        Me.pnInertia.Controls.Add(Me.Label51)
        Me.pnInertia.Controls.Add(Me.lblinertiaUnit)
        Me.pnInertia.Controls.Add(Me.tbInertia)
        Me.pnInertia.Location = New System.Drawing.Point(227, 108)
        Me.pnInertia.Name = "pnInertia"
        Me.pnInertia.Size = New System.Drawing.Size(298, 26)
        Me.pnInertia.TabIndex = 102
        '
        'lblinertiaUnit
        '
        Me.lblinertiaUnit.AutoSize = true
        Me.lblinertiaUnit.Location = New System.Drawing.Point(254, 6)
        Me.lblinertiaUnit.Name = "lblinertiaUnit"
        Me.lblinertiaUnit.Size = New System.Drawing.Size(36, 13)
        Me.lblinertiaUnit.TabIndex = 64
        Me.lblinertiaUnit.Text = "[kgm²]"
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel7.Controls.Add(Me.cbDifferentialIncluded)
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(12, 186)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(210, 24)
        Me.FlowLayoutPanel7.TabIndex = 106
        '
        'pnThermalOverloadRecovery
        '
        Me.pnThermalOverloadRecovery.Controls.Add(Me.Label52)
        Me.pnThermalOverloadRecovery.Controls.Add(Me.tbThermalOverload)
        Me.pnThermalOverloadRecovery.Controls.Add(Me.Label10)
        Me.pnThermalOverloadRecovery.Location = New System.Drawing.Point(227, 136)
        Me.pnThermalOverloadRecovery.Name = "pnThermalOverloadRecovery"
        Me.pnThermalOverloadRecovery.Size = New System.Drawing.Size(298, 23)
        Me.pnThermalOverloadRecovery.TabIndex = 103
        '
        'Label10
        '
        Me.Label10.AutoSize = true
        Me.Label10.Location = New System.Drawing.Point(254, 3)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(16, 13)
        Me.Label10.TabIndex = 67
        Me.Label10.Text = "[-]"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LbStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 565)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(864, 22)
        Me.StatusStrip1.SizingGrip = false
        Me.StatusStrip1.TabIndex = 74
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'LbStatus
        '
        Me.LbStatus.Name = "LbStatus"
        Me.LbStatus.Size = New System.Drawing.Size(39, 17)
        Me.LbStatus.Text = "Status"
        '
        'CmOpenFile
        '
        Me.CmOpenFile.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.CmOpenFile.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenWithToolStripMenuItem, Me.ShowInFolderToolStripMenuItem})
        Me.CmOpenFile.Name = "CmOpenFile"
        Me.CmOpenFile.Size = New System.Drawing.Size(153, 48)
        '
        'OpenWithToolStripMenuItem
        '
        Me.OpenWithToolStripMenuItem.Name = "OpenWithToolStripMenuItem"
        Me.OpenWithToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.OpenWithToolStripMenuItem.Text = "Open with ..."
        '
        'ShowInFolderToolStripMenuItem
        '
        Me.ShowInFolderToolStripMenuItem.Name = "ShowInFolderToolStripMenuItem"
        Me.ShowInFolderToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ShowInFolderToolStripMenuItem.Text = "Show in Folder"
        '
        'pnRatedPower
        '
        Me.pnRatedPower.Controls.Add(Me.lblRatedPower)
        Me.pnRatedPower.Controls.Add(Me.lblRatedPowerUnit)
        Me.pnRatedPower.Controls.Add(Me.tbRatedPower)
        Me.pnRatedPower.Location = New System.Drawing.Point(12, 136)
        Me.pnRatedPower.Name = "pnRatedPower"
        Me.pnRatedPower.Size = New System.Drawing.Size(210, 23)
        Me.pnRatedPower.TabIndex = 103
        '
        'lblRatedPower
        '
        Me.lblRatedPower.AutoSize = true
        Me.lblRatedPower.Location = New System.Drawing.Point(4, 4)
        Me.lblRatedPower.Name = "lblRatedPower"
        Me.lblRatedPower.Size = New System.Drawing.Size(69, 13)
        Me.lblRatedPower.TabIndex = 45
        Me.lblRatedPower.Text = "Rated Power"
        '
        'lblRatedPowerUnit
        '
        Me.lblRatedPowerUnit.AutoSize = true
        Me.lblRatedPowerUnit.Location = New System.Drawing.Point(165, 4)
        Me.lblRatedPowerUnit.Name = "lblRatedPowerUnit"
        Me.lblRatedPowerUnit.Size = New System.Drawing.Size(30, 13)
        Me.lblRatedPowerUnit.TabIndex = 64
        Me.lblRatedPowerUnit.Text = "[kW]"
        '
        'tbRatedPower
        '
        Me.tbRatedPower.Location = New System.Drawing.Point(102, 1)
        Me.tbRatedPower.Name = "tbRatedPower"
        Me.tbRatedPower.Size = New System.Drawing.Size(57, 20)
        Me.tbRatedPower.TabIndex = 2
        '
        'pnElectricMachineType
        '
        Me.pnElectricMachineType.Controls.Add(Me.cbEmType)
        Me.pnElectricMachineType.Controls.Add(Me.lblEmType)
        Me.pnElectricMachineType.Location = New System.Drawing.Point(12, 108)
        Me.pnElectricMachineType.Name = "pnElectricMachineType"
        Me.pnElectricMachineType.Size = New System.Drawing.Size(210, 26)
        Me.pnElectricMachineType.TabIndex = 108
        '
        'cbEmType
        '
        Me.cbEmType.FormattingEnabled = true
        Me.cbEmType.Location = New System.Drawing.Point(64, 3)
        Me.cbEmType.Name = "cbEmType"
        Me.cbEmType.Size = New System.Drawing.Size(130, 21)
        Me.cbEmType.TabIndex = 26
        '
        'lblEmType
        '
        Me.lblEmType.AutoSize = true
        Me.lblEmType.Location = New System.Drawing.Point(4, 5)
        Me.lblEmType.Name = "lblEmType"
        Me.lblEmType.Size = New System.Drawing.Size(50, 13)
        Me.lblEmType.TabIndex = 25
        Me.lblEmType.Text = "EM Type"
        '
        'cbNumWheelMotors
        '
        Me.cbNumWheelMotors.FormattingEnabled = true
        Me.cbNumWheelMotors.Location = New System.Drawing.Point(214, 2)
        Me.cbNumWheelMotors.Name = "cbNumWheelMotors"
        Me.cbNumWheelMotors.Size = New System.Drawing.Size(33, 21)
        Me.cbNumWheelMotors.TabIndex = 67
        '
        'IEPCForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(864, 587)
        Me.Controls.Add(Me.pnElectricMachineType)
        Me.Controls.Add(Me.pnRatedPower)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.FlowLayoutPanel7)
        Me.Controls.Add(Me.FlowLayoutPanel5)
        Me.Controls.Add(Me.pnThermalOverloadRecovery)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.pnInertia)
        Me.Controls.Add(Me.FlowLayoutPanel4)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.ButCancel)
        Me.Controls.Add(Me.ButOK)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.tcVoltageLevels)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.MaximizeBox = false
        Me.Name = "IEPCForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "IEPC Editor"
        Me.tcVoltageLevels.ResumeLayout(false)
        Me.tpFirstVoltageLevel.ResumeLayout(false)
        Me.FlowLayoutPanel9.ResumeLayout(false)
        Me.FlowLayoutPanel9.PerformLayout
        Me.TableLayoutPanel1.ResumeLayout(false)
        Me.TableLayoutPanel1.PerformLayout
        Me.GroupBox2.ResumeLayout(false)
        Me.TableLayoutPanel6.ResumeLayout(false)
        Me.TableLayoutPanel6.PerformLayout
        Me.tpVoltageLevel.ResumeLayout(false)
        Me.GroupBox4.ResumeLayout(false)
        Me.TableLayoutPanel2.ResumeLayout(false)
        Me.TableLayoutPanel2.PerformLayout
        Me.FlowLayoutPanel10.ResumeLayout(false)
        Me.FlowLayoutPanel10.PerformLayout
        Me.TableLayoutPanel4.ResumeLayout(false)
        Me.TableLayoutPanel4.PerformLayout
        Me.GroupBox1.ResumeLayout(false)
        Me.TableLayoutPanel5.ResumeLayout(false)
        Me.TableLayoutPanel5.PerformLayout
        Me.FlowLayoutPanel2.ResumeLayout(false)
        Me.GroupBox3.ResumeLayout(false)
        Me.TableLayoutPanel3.ResumeLayout(false)
        Me.TableLayoutPanel3.PerformLayout
        Me.FlowLayoutPanel11.ResumeLayout(false)
        Me.ToolStrip1.ResumeLayout(false)
        Me.ToolStrip1.PerformLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).EndInit
        Me.FlowLayoutPanel1.ResumeLayout(false)
        Me.FlowLayoutPanel1.PerformLayout
        Me.FlowLayoutPanel4.ResumeLayout(false)
        Me.FlowLayoutPanel4.PerformLayout
        Me.FlowLayoutPanel5.ResumeLayout(false)
        Me.FlowLayoutPanel5.PerformLayout
        Me.pnInertia.ResumeLayout(false)
        Me.pnInertia.PerformLayout
        Me.FlowLayoutPanel7.ResumeLayout(false)
        Me.FlowLayoutPanel7.PerformLayout
        Me.pnThermalOverloadRecovery.ResumeLayout(false)
        Me.pnThermalOverloadRecovery.PerformLayout
        Me.StatusStrip1.ResumeLayout(false)
        Me.StatusStrip1.PerformLayout
        Me.CmOpenFile.ResumeLayout(false)
        Me.pnRatedPower.ResumeLayout(false)
        Me.pnRatedPower.PerformLayout
        Me.pnElectricMachineType.ResumeLayout(false)
        Me.pnElectricMachineType.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
	Friend WithEvents btFLCurveFile1 As Button
	Friend WithEvents tbThermalOverload As TextBox
	Friend WithEvents Label52 As Label
	Friend WithEvents Label48 As Label
	Friend WithEvents Label51 As Label
	Friend WithEvents tbInertia As TextBox
	Friend WithEvents Label49 As Label
	Friend WithEvents tbModel As TextBox
	Friend WithEvents tcVoltageLevels As TabControl
	Friend WithEvents tpFirstVoltageLevel As TabPage
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents lvPowerMap1 As ListView
	Friend WithEvents ColumnHeader13 As ColumnHeader
	Friend WithEvents ColumnHeader14 As ColumnHeader
	Friend WithEvents Label47 As Label
	Friend WithEvents Label44 As Label
	Friend WithEvents Label43 As Label
	Friend WithEvents tbOverloadTime1 As TextBox
	Friend WithEvents Label42 As Label
	Friend WithEvents tboverloadTorqueSpeed1 As TextBox
	Friend WithEvents Label41 As Label
	Friend WithEvents tbOverloadTorque1 As TextBox
	Friend WithEvents tbContinousTorqueSpeed1 As TextBox
	Friend WithEvents Label39 As Label
	Friend WithEvents tbContinousTorque1 As TextBox
	Friend WithEvents Label37 As Label
	Friend WithEvents tbVoltage1 As TextBox
	Friend WithEvents tpVoltageLevel As TabPage
	Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
	Friend WithEvents Label1 As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents cbDesignTypeWheelMotor As CheckBox
	Friend WithEvents cbDifferentialIncluded As CheckBox
	Friend WithEvents Label4 As Label
	Friend WithEvents Label12 As Label
	Friend WithEvents Label11 As Label
	Friend WithEvents Label14 As Label
	Friend WithEvents Label13 As Label
	Friend WithEvents Label15 As Label
	Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
	Friend WithEvents Label16 As Label
	Friend WithEvents Label17 As Label
	Friend WithEvents Label18 As Label
	Friend WithEvents Label19 As Label
	Friend WithEvents tbVoltage2 As TextBox
	Friend WithEvents Label20 As Label
	Friend WithEvents Label21 As Label
	Friend WithEvents tbContinousTorque2 As TextBox
	Friend WithEvents tbContinousTorqueSpeed2 As TextBox
	Friend WithEvents tbOverloadTorque2 As TextBox
	Friend WithEvents tbOverloadTorqueSpeed2 As TextBox
	Friend WithEvents tbOverloadTime2 As TextBox
	Friend WithEvents Label22 As Label
	Friend WithEvents Label23 As Label
	Friend WithEvents Label24 As Label
	Friend WithEvents Label25 As Label
	Friend WithEvents Label26 As Label
	Friend WithEvents Label27 As Label
	Friend WithEvents tbFLCurve2 As TextBox
	Friend WithEvents btFLCurveFile2 As Button
	Friend WithEvents Label28 As Label
	Friend WithEvents TableLayoutPanel6 As TableLayoutPanel
	Friend WithEvents tbFLCurve1 As TextBox
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents btAddGear As Button
	Friend WithEvents btRemoveGear As Button
	Friend WithEvents lvGear As ListView
	Friend WithEvents ColumnHeader1 As ColumnHeader
	Friend WithEvents ColumnHeader2 As ColumnHeader
	Friend WithEvents ColumnHeader3 As ColumnHeader
	Friend WithEvents GroupBox3 As GroupBox
	Friend WithEvents btAddDragCurve As Button
	Friend WithEvents btRemoveDragCurve As Button
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripBtNew As ToolStripButton
    Friend WithEvents ToolStripBtOpen As ToolStripButton
    Friend WithEvents ToolStripBtSave As ToolStripButton
    Friend WithEvents ToolStripBtSaveAs As ToolStripButton
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ToolStripBtSendTo As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents lblTitle As Label
    Friend WithEvents ButCancel As Button
    Friend WithEvents ButOK As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As Panel
    Friend WithEvents pnInertia As Panel
    Friend WithEvents FlowLayoutPanel7 As Panel
    Friend WithEvents FlowLayoutPanel9 As FlowLayoutPanel
    Friend WithEvents lblinertiaUnit As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents FlowLayoutPanel10 As FlowLayoutPanel
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Label5 As Label
    Friend WithEvents lvPowerMap2 As ListView
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents LbStatus As ToolStripStatusLabel
    Friend WithEvents TableLayoutPanel5 As TableLayoutPanel
    Friend WithEvents Label6 As Label
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lvDragCurve As ListView
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents FlowLayoutPanel11 As FlowLayoutPanel
    Friend WithEvents Label7 As Label
    Friend WithEvents btShowFLCurve1 As Button
    Friend WithEvents btShowFLCurve2 As Button
    Friend WithEvents CmOpenFile As ContextMenuStrip
    Friend WithEvents OpenWithToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowInFolderToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents pnRatedPower As Panel
    Friend WithEvents lblRatedPower As Label
    Friend WithEvents lblRatedPowerUnit As Label
    Friend WithEvents tbRatedPower As TextBox
    Friend WithEvents pnElectricMachineType As Panel
    Friend WithEvents cbEmType As ComboBox
    Friend WithEvents lblEmType As Label
    Friend WithEvents FlowLayoutPanel5 As Panel
    Friend WithEvents pnThermalOverloadRecovery As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents cbNumWheelMotors As ComboBox
End Class
